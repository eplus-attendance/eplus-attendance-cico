import os
import re
from datetime import datetime

class Logger():
    def __init__(self, filePath):
        """Initializes the logger class

        Arguments:
            filePath {string} -- The path to the log file
        """

        self.setFilePath(filePath)
        self.writeToConsole = False

    def setFilePath(self, filePath):
        self.filePath = filePath

    def log(self, line):
        """Logs a line to the log file

        Arguments:
            line {Log/string} -- A string containing the line to log
        """

        self.createDirectoryIfMissing()

        stringToLog = str(line)

        with open(self.filePath, 'a+') as fileHandle:
            fileHandle.write(stringToLog)

        if self.writeToConsole:
            print(stringToLog)

    def createDirectoryIfMissing(self):
        os.makedirs(
            os.path.dirname(self.filePath),
            exist_ok = True
        )

    def lastLog(self):
        """Returns the last line of the log file

        Returns:
            string -- The last logged line
        """

        with open(self.filePath, 'r') as fileHandle:
            logLines = fileHandle.read().splitlines()
            lastLog = Log.unpack(logLines[-1])
            return lastLog


class Log():
    def __init__(self, body=None):
        """Initializes Log, to use as a log line

        Keyword Arguments:
            body {string} -- The body of the log (default: {None})
        """

        self.DATETIME_FORMAT = '%Y-%m-%d %H:%M:%S'

        if body:
            self.createdAt = datetime.now().strftime(self.DATETIME_FORMAT)
            self.body = body

    def __str__(self):
        return '[{0}] {1}'.format(
            self.createdAt,
            self.body,
        )

    @staticmethod
    def unpack(log):
        """Unpacks a log line into a Log object

        Arguments:
            log {string} -- The log line

        Returns:
            Log -- An instance of Log
        """

        regex = re.match('\[(.+)\] (.+)', log)
        dateTimeString = regex.group(1)
        body = regex.group(2)

        log = Log()
        log.setCreatedAt(dateTimeString)
        log.body = body

        return log

    def setCreatedAt(self, dateTime):
        self.createdAt = datetime.strptime(
            dateTime,
            self.DATETIME_FORMAT
        )
