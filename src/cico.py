#!/usr/bin/python3

from logger import Logger
from logger import Log
import RPi.GPIO as GPIO
import Rfid
import requests
import signal

def api(path):
    # API_ROOT = 'https://httpbin.org/post'
    API_ROOT = 'https://app.eplus-attendance.ga/api/v1'
    return API_ROOT + path

if __name__ == '__main__':
    logger = Logger('logs/cico-001.txt')
    logger.writeToConsole = True

    logger.log(Log('Program started.'))

    continueReadingMfrc = True

    # Capture SIGINT for cleanup when the script is aborted
    def endRead(signal, frame):
        global continueReadingMfrc
        print("Ctrl+C captured, ending read.")
        continueReadingMfrc = False
        GPIO.cleanup()

    # Hook the SIGINT
    signal.signal(signal.SIGINT, endRead)

    # Create an object of the class Rfid
    # Correct values for the Raspberry Pi
    MIFAREReader = Rfid.Reader(0, 0, 22)

    while continueReadingMfrc:
        # Scan for cards
        (status,TagType) = MIFAREReader.MFRC522_Request(MIFAREReader.PICC_REQIDL)

        # Get the UID of the card
        (status,uid) = MIFAREReader.MFRC522_Anticoll()

        # If we have the UID, continue
        if status == MIFAREReader.MI_OK:
            # Print UID
            print("Card read UID: " + str(uid[0]) + "," + str(uid[1]) + "," + str(uid[2]) + "," + str(uid[3]))

            # This is the default key for authentication
            key = [0xFF, 0xFF, 0xFF, 0xFF, 0xFF, 0xFF]

            # Select the scanned tag
            MIFAREReader.MFRC522_SelectTag(uid)

            # Authenticate
            status = MIFAREReader.MFRC522_Auth(MIFAREReader.PICC_AUTHENT1A, 8, key, uid)

            # Check if authenticated
            if status == MIFAREReader.MI_OK:
                addr, data = MIFAREReader.MFRC522_Read(8)
                print("Sector " + str(addr) + " " + str(data))

                MIFAREReader.MFRC522_StopCrypto1()
            else:
                print("Authentication error")

            response = requests.post(api('/calendar'), data = {
                'card_uid': 'AABBCCDD',
                'cico_device_id': CICO_DEVICE_ID,
            })

            print(response)

    logger.log(Log('Program exited.'))
