import io
import sys
import re
from src import Logger
from src import Log

class TestLogger():
    def test_filePath(self):
        expected = 'logs/test-000.txt'
        logger = Logger(expected)
        received = logger.filePath
        assert received == expected

    def test_log(self):
        logger = Logger('logs/test-001.txt')

        expected = 'abc'
        logger.log(Log(expected))

        received = logger.lastLog().body

        assert received == expected

    def test_writeToConsole(self):
        logger = Logger('logs/test-002.txt')

        capturedOutput = io.StringIO()
        sys.stdout = capturedOutput # Redirect stdout

        logger.log(Log('DO NOT write to console.'))

        logger.writeToConsole = True
        logger.log(Log('DO write to console.'))

        logger.writeToConsole = False
        logger.log(Log('DO NOT write to console.'))

        sys.stdout = sys.__stdout__ # Reset stdout redirection

        received = capturedOutput.getvalue()

        assert bool(re.match('\[.+\] DO write to console.\n', received))
